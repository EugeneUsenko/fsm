## Description

This repository contains table based implementation of the FSM(finite-state machine) for the turnstile, described in [example][wiki_ref] section.

![Image](http://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Turnstile_state_machine_colored.svg/500px-Turnstile_state_machine_colored.svg.png)

[wiki_ref]:http://en.wikipedia.org/wiki/Finite-state_machine#Example:_a_turnstile