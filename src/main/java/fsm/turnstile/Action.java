package fsm.turnstile;

public enum Action {
    NONE,
    LOCK_TURNSTILE,
    RELEASE_TURNSTILE;

    private ActionDelegate mActionDelegate;

    public void setActionDelegate(ActionDelegate delegate) {
        mActionDelegate = delegate;
    }

    public void perform() {
        if (mActionDelegate == null) return;
        mActionDelegate.perform();
    }
}
