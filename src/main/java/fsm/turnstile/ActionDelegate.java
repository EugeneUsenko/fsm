package fsm.turnstile;

public interface ActionDelegate {
    void perform();
}
