package fsm.turnstile;


import java.util.Collection;
import java.util.HashSet;
import java.util.logging.Logger;

public class FSM {
    private static final String TAG = FSM.class.getName();
    private static Logger log = Logger.getLogger(TAG);


    private Collection<Transition> mTransitions = new HashSet<Transition>();
    private State mCurrentState = State.LOCKED;

    {
        add(State.LOCKED, Event.COIN, State.UNLOCKED, Action.RELEASE_TURNSTILE);
        add(State.LOCKED, Event.PUSH, State.LOCKED, Action.NONE);
        add(State.UNLOCKED, Event.COIN, State.UNLOCKED, Action.NONE);
        add(State.UNLOCKED, Event.PUSH, State.LOCKED, Action.LOCK_TURNSTILE);
    }

    public void onEvent(Event event) {
        if (event == null) {
            throw new IllegalArgumentException("event can't be null");
        }

        log.info("received: event=" + event + ", state=" + mCurrentState);
        for (Transition transition : mTransitions) {
            if (mCurrentState == transition.current && event == transition.event) {
                log.info("  change: " + mCurrentState + " -> " + transition.output
                        + ", action=" + transition.action);
                transition.action.perform();
                mCurrentState = transition.output;
                return;
            }
        }
        log.info(" skipped: event=" + event + ", state=" + mCurrentState);
//        throw new RuntimeException("undefined transition: event=" + event + ", state=" + mCurrentState);
    }

    public State getSate() {
        return mCurrentState;
    }

    /* for testing purpose */
    void setState(State state) {
        mCurrentState = state;
    }

    private void add(State in, Event event, State out, Action action) {
        int size = mTransitions.size();

        Transition transition = new Transition(in, event, out, action);
        mTransitions.add(transition);

        if (size == mTransitions.size()) {
            throw new IllegalArgumentException("transition " + transition + " already exists");
        }
    }

    private final class Transition {
        final State current;
        final Event event;
        final State output;
        final Action action;

        Transition(State current, Event event, State output, Action action) {
            this.current = current;
            this.event = event;
            this.output = output;
            this.action = action;
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();
            sb.append("current=").append(current);
            sb.append("event=").append(event);
            sb.append("output=").append(output);
            sb.append("action=").append(action);
            return sb.toString();
        }

        /**
         * For transition only <code>current</code> state and <code>event</code>
         * does matter. The rest fields will not be taken into account.
         */
        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Transition that = (Transition) o;

            if (event != that.event) return false;
            if (current != that.current) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = current != null ? current.hashCode() : 0;
            result = 31 * result + (event != null ? event.hashCode() : 0);
            return result;
        }
    }
}
