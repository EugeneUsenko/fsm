package fsm.turnstile;


public enum State {
    LOCKED,
    UNLOCKED;
}
