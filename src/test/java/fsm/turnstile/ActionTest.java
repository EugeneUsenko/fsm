package fsm.turnstile;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class ActionTest {

    @Before
    public void up() {
        for (Action action : Action.values()) {
            action.setActionDelegate(null);
        }
    }

    @Test
    public void testPerform() {
        StateAwareAction delegate = new StateAwareAction();
        Action.LOCK_TURNSTILE.setActionDelegate(delegate);

        assertFalse(delegate.performed);
        Action.LOCK_TURNSTILE.perform();
        assertTrue(delegate.performed);
    }
}
