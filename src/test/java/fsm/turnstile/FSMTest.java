package fsm.turnstile;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FSMTest {
    static StateAwareAction sReleaseActionDelegate;
    static StateAwareAction sLockActionDelegate;
    static StateAwareAction sNoneActionDelegate;

    @BeforeClass
    public static void upClass() {
        sReleaseActionDelegate = new StateAwareAction();
        sLockActionDelegate = new StateAwareAction();
        sNoneActionDelegate = new StateAwareAction();

        Action.LOCK_TURNSTILE.setActionDelegate(sLockActionDelegate);
        Action.RELEASE_TURNSTILE.setActionDelegate(sReleaseActionDelegate);
        Action.NONE.setActionDelegate(sNoneActionDelegate);
    }

    @Before
    public void up() {
        sReleaseActionDelegate.reset();
        sLockActionDelegate.reset();
        sNoneActionDelegate.reset();
    }

    @Test
    public void testCoinOnLocked() {
        FSM fsm = new FSM();
        fsm.setState(State.LOCKED);
        fsm.onEvent(Event.COIN);

        assertEquals(State.UNLOCKED, fsm.getSate());
        assertTrue(sReleaseActionDelegate.performed);
        assertFalse(sLockActionDelegate.performed);
        assertFalse(sNoneActionDelegate.performed);
    }

    @Test
    public void testCoinOnUnlocked() {
        FSM fsm = new FSM();
        fsm.setState(State.UNLOCKED);
        fsm.onEvent(Event.COIN);

        assertEquals(State.UNLOCKED, fsm.getSate());
        assertFalse(sReleaseActionDelegate.performed);
        assertFalse(sLockActionDelegate.performed);
        assertTrue(sNoneActionDelegate.performed);
    }

    @Test
    public void testPushOnLocked() {
        FSM fsm = new FSM();
        fsm.setState(State.LOCKED);
        fsm.onEvent(Event.PUSH);

        assertEquals(State.LOCKED, fsm.getSate());
        assertFalse(sReleaseActionDelegate.performed);
        assertFalse(sLockActionDelegate.performed);
        assertTrue(sNoneActionDelegate.performed);
    }

    @Test
    public void testPushOnUnlocked() {
        FSM fsm = new FSM();
        fsm.setState(State.UNLOCKED);
        fsm.onEvent(Event.PUSH);

        assertEquals(State.LOCKED, fsm.getSate());
        assertFalse(sReleaseActionDelegate.performed);
        assertTrue(sLockActionDelegate.performed);
        assertFalse(sNoneActionDelegate.performed);
    }
}
