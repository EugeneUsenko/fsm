package fsm.turnstile;

final class StateAwareAction implements ActionDelegate {
    boolean performed = false;

    @Override
    public void perform() {
        performed = true;
    }

    public void reset() {
        performed = false;
    }
}
